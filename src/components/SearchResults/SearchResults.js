
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';

function SearchResults (props) {
  const q = props.query;
  let [searchResults, setSearchResults] = useState(null);

  console.log('Search results created: ' + q);

  const makeRequest = async (q) => {
    const url = 'https://dcrawl-back.herokuapp.com/api/Discord_Server/?guild_name=' + q;
    const options = {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
      }
    };

    const res = await fetch(url);
    const responseOK = res && res.status === 200 && res.statusText === 'OK';
    if (responseOK) {
      const response = await res.json();
      console.log('Response: ' + response);
      const ctobj = Object.keys(response).length;
      console.log('Result count: ' + ctobj);

      const resultList = response.map(server => {
        console.log(server);
        return {
          name: server.guild_name,
          id: server.id
        };
      });

      console.log(resultList);

      setSearchResults(resultList);
    }
  };

  useEffect(() => {
    makeRequest(q);
  }, [q]);


  const getURLForQuery = q => {
    return ('/?q=' + q);
  };

  const buildSearchResultsList = serverList => {
    let eleList = [];
    eleList.push(
      <li key={'0000'}>
        <a
          href={getURLForQuery(q)}
          active="false"
          className="hasIcon">
          <i className="icon fas fa-search Button-icon" />
            See more results for <i>{q}</i>
        </a>
      </li>
    );

    serverList.forEach(server => {
      eleList.push(
        <li key={server.id}>
          <a className="hasIcon" active="false"><i className="icon fas fa-search Button-icon" /> {server.name} </a>
        </li>
      );
    });

    return eleList;
  };

  return (
    <ul className="Dropdown-menu Search-results">
      {searchResults ? buildSearchResultsList(searchResults) : <li> No results returned for <i> {q} </i></li>}
    </ul>
  );
}

export default SearchResults;
