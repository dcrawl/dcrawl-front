/* eslint-disable no-unused-vars */

import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import ServerCard from '../ServerCard/ServerCard';
import LoadingIndicator from '../Loading/Loading';
import './ServerCardGrid.css';
import UltimatePaginationMaterialUi from '../PaginationView/PaginationView';
import Fade from 'react-reveal/Zoom';

const Centered = styled.div `
  display: inline-block;
  text-align: center;
  margin-left: 0%;
  width: 100%;
`;

const ServerCardGrid = () => {
  let [serverList, setServerList] = useState([]);
  let [hasPulledServers, setPulled] = useState(false);
  let [serverCount, setServerCount] = useState(0);
  let [lastReceivedID, setLastReceivedID] = useState(0);
  let [activePage, setActivePage] = useState(1);

  function countObjects (obj) {
    return Object.keys(obj).length;
  }

  function handlePageChange (pageNumber) {
    setServerCount(0);
    setActivePage(pageNumber);
    setLastReceivedID(999999);
    setPulled(false);
  }

  useEffect(() => {
    if (serverCount === 0) {
      try {
        (async () => {
          let res = await fetch("https://dcrawl-back.herokuapp.com/api/Discord_Server/?page=" + activePage);
          let response = await res.json();
          setServerList(response.results);
          console.log('Results: ' + response.results);
          var lastID = 0;
          lastID = response.results[0].id;
          setServerCount(response.count);
          setPulled(true);
          setLastReceivedID(lastID);
        })();
      } catch (e) {
        console.log(e);
      }
    }
  }, [lastReceivedID]);


  function renderLoadingIndicator () {
    if (!hasPulledServers) {
      return (
        <Centered>
          <LoadingIndicator isLoading>Loading</LoadingIndicator>
        </Centered>
      );
    }
  }

  function renderPaginator () {
    if (hasPulledServers) {
      return (
        <Fade duration={100}>
          <Centered>
            <UltimatePaginationMaterialUi totalPages={154} currentPage={activePage} onChange={handlePageChange} />
          </Centered>
        </Fade>
      );
    }
  }

  function renderGrid (page) {
    var paginateServers = [];
    if (serverList && serverCount > 0) {
      var count = 0;
      serverList.map((server) => {
        count++;
        paginateServers.unshift(<li key={count}>
          <ServerCard
            cardNumber={count}
            key={count}
            code={server['invite_link']}
            data={server}
          /></li>);
      });
    }
    return (
      <div>
        { renderPaginator() }
        <ul>
          { paginateServers }
        </ul>
        { renderPaginator() }
      </div>
    );
  }


  return (
    <div>
      { renderLoadingIndicator() }
      <div className="servergrid">
        { renderGrid() }
      </div>
    </div>
  );
};


export default ServerCardGrid;
