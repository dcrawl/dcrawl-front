import React from 'react';
import ServerCardGrid from '../ServerCardGrid/ServerCardGrid';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import _ from 'lodash';

console.log(_.join(['Another', 'module', 'loaded!'], ' '));

// homepage
function Homepage () {
  return (
    <MuiThemeProvider>
      <div className="wrapper">
        <ServerCardGrid />
      </div>
    </MuiThemeProvider>
  );
}

export default Homepage;
