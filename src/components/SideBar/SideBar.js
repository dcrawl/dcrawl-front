/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import '../styles/SideBar.css';

// TODO: Pass callback to ServerCardGrid onNext/onPrev for SPA re-render

function Sidebar () {
  return (
    <div className="sidenav">
      Filter
      <a href="#">Item 1</a>
      <a href="#">Item 2</a>
      <a href="#">Item 3</a>
      <a href="#">Item 4</a>
    </div>
  );
}

export default Sidebar;
