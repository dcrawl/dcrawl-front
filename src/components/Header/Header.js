
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import SearchResults from '../SearchResults/SearchResults';

const logo2 = require('../../assets/dcrawlericon_even.png');

function Header () {
  var [searchFocus, setSearchFocus] = useState(false);
  var [resultsOpen, setResultsOpen] = useState(false);
  var [searchQuery, setSearchQuery] = useState("");

  function onSearchChange (data) {
    setSearchQuery(data);
    if (data.length > 2) {
      setResultsOpen(true);
    } else {
      setResultsOpen(false);
    }
    //console.log('Results open: ' + resultsOpen + ' Query: ' + data);
  }


  function onSearchFocus () {
    //console.log('onSearchFocus');
    setSearchFocus(true);
  }

  function onSearchBlur () {
    //console.log('onSearchBlur');
    setSearchFocus(false);
  }

  function getSearchClass () {
    if (searchFocus && resultsOpen) {
      return 'Search focused open';
    } else if (searchFocus && !resultsOpen) {
      return 'Search focused';
    } else if (!searchFocus) {
      return 'Search';
    }
  }

  function renderSearchResults () {
    if (resultsOpen && searchQuery.length > 1) {
      //console.log('Rendering search results');
      return (
        <SearchResults query={searchQuery} />
      );
    }
  }

  return (
    <div className="root-drawer ">
      <header className="root-header">
        <div className="container">
          <h1 className="Header-title">
            <a href="http://discordcrawler.firebaseapp.com">
              <img src="https://media.moddb.com/images/articles/1/251/250026/Discord-logo.png" alt="Discord Crawler: Find Interesting Servers" className="Header-logo" />
            </a>&nbsp;&nbsp;Discord Crawler
          </h1>
          <div className="Header-primary">
            <ul className="Header-controls">
              <li className="item-link1" />
            </ul>
          </div>
          <div className="Header-secondary">
            <ul className="Header-controls">
              <li className="item-search">
                <div className={getSearchClass()} >
                  <div className="Search-input">
                    <input
                      className="FormControl"
                      type="search"
                      placeholder="Find Servers..."
                      onFocus={onSearchFocus}
                      onBlur={onSearchBlur}
                      onChange={e => onSearchChange(e.target.value)}/>
                  </div>
                  { renderSearchResults() }
                </div>
              </li>
            </ul>
          </div>
        </div>
      </header>
    </div>
  );
}

export default Header;
