/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import styled from 'styled-components';
import './ServerCard.css';
import Zoom from 'react-reveal/Zoom';

const CardBackground = styled.div.attrs(props => ({
  style: { background: 'url(' + `${props.bgImage}` + ')' }
}))`background-size: 100% !important`;

const CardJoin = styled.div`
  float: right;
  margin-top: 4em;
  margin-right: 1em;
  position: relative;`;


function ServerCard (props) {
  const [cardData, setCardData] = useState(props.data);
  const [cardNumber, setCardNumber] = useState(props.cardNumber);
  const [inviteCode, setInviteCode] = useState(props.code);

  function getJoinURL () {
    return (
      'https://discord.gg/' + inviteCode
    );
  }

  function getIconURL (size = 128) {
    if (cardData) {
      var guildID = cardData['guild_id'];
      var iconID = cardData['guild_icon'];
      if (iconID === 'undefined.png') {
        return 'https://media.moddb.com/images/articles/1/251/250026/Discord-logo.png';
      }
      return (
        'https://cdn.discordapp.com/icons/' +
        guildID +
        '/' +
        iconID +
        '.png?size=' +
        size
      );
    }
    return null;
  }

  function getCountRatio () {
    if (cardData && cardData.approximate_member_count) {
      let ratio = cardData.approximate_presence_count / cardData.approximate_member_count;
      return ratio;
    }
    return 0;
  }

  // 0.0=red to 1.0=green
  function getColor (value) {
    var hue = (value * 120).toString(10);
    return ["hsl(", hue, ",100%,50%)"].join("");
  }

  // most are 1536 px
  const getSplashURL = () => {
    return (
      'https://cdn.discordapp.com/splashes/' +
      cardData.guild_id +
      '/' +
      cardData.guild_splash +
      '.jpg?size=1536'
    );
  };

  const hasSplash = () => {
    if (cardData && (cardData.guild_splash === null || cardData.guild_splash === 'undefined')) {
      return false;
    } else {
      return true;
    }
    return false;
  };


  function renderBackground (cardIcon) {
    if (!hasSplash()) {
      return (
        <CardBackground className="BackgroundBlur" bgImage={cardIcon} />
      );
    } else {
      return (
        <CardBackground className="Background" bgImage={getSplashURL()} />
      );
    }
  }


  function renderCard () {
    if (cardData) {
      var cardIcon = getIconURL(128);
      return (
        <Zoom duration = {300}>
          <div className="CardTop">
            { renderBackground(cardIcon) }
            <div className="Avatar" style={{ background: 'url(' + cardIcon + ')', backgroundSize: '100%' }} />

            <div className="info">
              <h3>{cardData['guild_name']}</h3>
            </div><div className="rightbar" style = {{ background: 'linear-gradient (90deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 50%, rgba(252,176,69,1) 100%)' }}/>
          </div>
          <div className="main__content">
            <div className="upper__card">
              <div className="info2">
                <div className="pill">
                  <i className="fa fa-circle online" aria-hidden="true" />
                  { cardData ['approximate_presence_count'] } online <div className="spacer"/>
                </div><div className="spacer"/>
                <div className="pill">
                  <i className="fa fa-circle" aria-hidden="true" />
                  { cardData ['approximate_member_count'] } members <div className="spacer"/>
                </div><div className="spacer"/>
                <a href={getJoinURL()} className="pill pill__link">
                  <i className="fa fa-sign-in-alt" aria-hidden="true" />
                  Join Now
                </a>
              </div>
            </div>
          </div>
        </Zoom>
      );
    }
  }
  return <div>{renderCard()}</div>;
}

export default ServerCard;
