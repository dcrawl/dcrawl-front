import React, { Component } from 'react';

import { Route, HashRouter } from "react-router-dom";
import Home from './components/Homepage/Homepage';
import Header from './components/Header/Header';

import './styles/Globals.css';

class App extends Component {
  render () {
    return (
      <HashRouter>
        <div>
          <Header />
          <div className="content">
            <Route exact path="/" component={Home}/>

          </div>
        </div>
      </HashRouter>
    );
  }
}

export default App;
